import gym
from gym import error, spaces, utils
from gym.spaces import Box
from gym.spaces import Dict
from gym.utils import seeding
import numpy as np
import time
import vtk
import threading
import itertools
import copy
from gym_uav.envs.utils import TimerCallback
from gym_uav.envs.utils import Config
from gym_uav.envs.utils import Smoother


class UavGoalEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        print("using goal directed environment with sparse reward, the environment version is uav-v2")
        Common = Config()
        self.basic_directions = Common.basic_directions
        self.extra_directions = Common.extra_directions
        self.original_observation_length = 12
        self.extra_length = len(self.extra_directions)

        self.state_space = Box(-np.inf, np.inf, [self.original_observation_length + self.extra_length], float)
        self.observation_space = Dict({"observation": self.state_space,
                                       "achieved_goal": Box(-np.inf, np.inf, [2], float),
                                       "desired_goal": Box(-np.inf, np.inf, [2], float)})

        self.action_space = Box(-1.0, 1.0, [2], float)
        self._env_step_counter = 0
        self.reward_type = "sparse"

        self.state = np.zeros([self.state_space.shape[0]])

        # 无人机参数
        self.level = Common.level
        self.position = np.zeros([2])
        self.target = np.zeros([2])
        self.orient = np.zeros([1])
        self.speed = np.zeros([1])  # 定义无人机的初始速度为0.0
        self.max_speed = Common.max_speed
        self.min_distance_to_target = Common.min_distance_to_target
        self.real_action_range = Common.real_action_range

        # self.File = open('./path_agent.txt', 'w+')

        # 环境参数
        self.min_distance_to_obstacle = Common.min_distance_to_obstacle
        self.min_initial_starts = Common.min_initial_starts
        self.expand = Common.expand
        self.num_circle = Common.num_circle
        self.radius = Common.radius
        self.period = Common.period
        self.mat_height = None
        self.mat_exist = None

        # range finder parameters
        self.scope = Common.scope
        self.min_step = Common.min_step
        self.directions = self.basic_directions + self.extra_directions
        self.end_points = [None for _ in range(len(self.directions))]

        # goal directed observation
        self.observation = {'observation': None, 'achieved_goal': None, 'desired_goal': None}

        # rendering parameters
        self.margin = Common.margin
        self.env_params = {'cylinders': None, 'size': 1.5*(self.num_circle+self.margin*2)*self.period}
        self.agent_params = {'position': self.position, 'target': self.target, 'direction':None, 'rangefinders': self.end_points}
        self.agent_params_pre = None
        self.first_render = True
        self.terminate_render = False

        # other parameters
        self.is_reset = False

        assert self.scope > self.max_speed
        assert self.reward_type == "dense" or self.reward_type == "sparse"

    def _fast_range_finder(self, position, theta, forward_dist, min_dist=0.0, find_type='normal'):
        # print("steps and forward", forward_dist, self.min_step, forward_dist)
        end_cache = copy.deepcopy(position)
        position_integer = np.floor(end_cache / self.period).astype(np.int)
        judge = end_cache - (position_integer * self.period + self.period / 2)
        # print("end_cache", end_cache, "position_integer", position_integer, judge)
        if judge[0] >= 0 and judge[1] > 0:
            # print("in one")
            down_left = position_integer * self.period + self.period / 2
            down_right = (position_integer + np.array([1, 0])) * self.period + self.period / 2
            up_left = (position_integer + np.array([0, 1])) * self.period + self.period / 2
            up_right = (position_integer + np.array([1, 1])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] + 1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] + self.expand, position_integer[1] + 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + 1 + self.expand, position_integer[1] + 1 + self.expand]]))
        elif judge[0] >= 0 and judge[1] < 0:
            # print('in two')
            down_left = (position_integer + np.array([0, -1])) * self.period + self.period / 2
            down_right = (position_integer + np.array([1, -1])) * self.period + self.period / 2
            up_left = position_integer * self.period + self.period / 2
            up_right = (position_integer + np.array([1, 0])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[position_integer[0] + 1 + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + 1 + self.expand, position_integer[1] + self.expand]]))
        elif judge[0] < 0 and judge[1] > 0:
            # print("in three")
            down_left = (position_integer + np.array([-1, 0])) * self.period + self.period / 2
            down_right = position_integer * self.period + self.period / 2
            up_left = (position_integer + np.array([-1, 1])) * self.period + self.period / 2
            up_right = (position_integer + np.array([0, 1])) * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] -1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[position_integer[0] - 1 + self.expand, position_integer[1] + 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + 1 + self.expand]]))
        else:
            # print("in four")
            down_left = (position_integer + np.array([-1, -1])) * self.period + self.period / 2
            down_right = (position_integer + np.array([0, -1])) * self.period + self.period / 2
            up_left = (position_integer + np.array([-1, 0])) * self.period + self.period / 2
            up_right = position_integer * self.period + self.period / 2
            exists = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'],
                              [self.mat_exist[position_integer[0] - 1 + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] - 1 + self.expand],
                               self.mat_exist[
                                   position_integer[0] - 1 + self.expand, position_integer[1] + self.expand],
                               self.mat_exist[
                                   position_integer[0] + self.expand, position_integer[1] + self.expand]]))

        base_points = dict(zip(['down_left', 'down_right', 'up_left', 'up_right'], [down_left, down_right, up_left, up_right]))

        # print(base_points, end_cache)
        dist = []
        end = []
        for base in base_points.keys():
            theta_base = np.arctan(np.abs((base_points[base] - end_cache)[0] / (base_points[base] - end_cache)[1]))
            if base == 'down_left':
                theta_base = np.pi + theta_base
            if base == 'down_right':
                theta_base = np.pi - theta_base
            if base == 'up_left':
                theta_base = 2 * np.pi - theta_base
            if base == 'up_right':
                theta_base = theta_base

            theta_base = np.mod(theta_base, 2*np.pi)

            dist_to_base = np.linalg.norm(end_cache - base_points[base])

            delta_theta = theta - theta_base
            if dist_to_base - (self.radius + min_dist) >= forward_dist or exists[base] < 0:
                dist.append(1.0)
                end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                # print("pass I")
            elif (dist_to_base - (self.radius + min_dist) >= 0) and (np.cos(delta_theta) <= 0):
                dist.append(1.0)
                end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                # print("pass II")
            else:
                # print("in III")
                min_dist_to_origin = np.abs(np.sin(delta_theta)) * dist_to_base

                if min_dist_to_origin >= (self.radius + min_dist):
                    dist.append(1.0)
                    end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                else:
                    # print(base, 'min_dist_to_origin', min_dist_to_origin)

                    dist_inner = np.sqrt((self.radius + min_dist) ** 2 - min_dist_to_origin ** 2)
                    # theta_inner = np.arccos(min_dist_to_origin / (self.radius + min_dist))
                    # print(base, 'theta_inner', theta_inner / np.pi * 180)
                    # dist_inner = (self.radius + min_dist) * np.sin(theta_inner)
                    # print(base, 'dist_inner', dist_inner, 'dist_to_base', dist_to_base)

                    final_dist = np.cos(delta_theta) * dist_to_base - dist_inner
                    final_dist = final_dist[0]
                    # print(base, 'final_dist', final_dist)
                    if final_dist >= forward_dist:
                        dist.append(1.0)
                        end.append(end_cache + np.array([forward_dist * np.sin(theta[0]), forward_dist * np.cos(theta[0])]))
                    else:
                        # print("in final", final_dist)
                        dist.append(final_dist / forward_dist)
                        end.append(end_cache + np.array([final_dist * np.sin(theta[0]), final_dist * np.cos(theta[0])]))
        dist = np.array(dist)

        return np.min(dist), end[np.argmin(dist)]

    def _range_finder(self, position, theta, steps, min_dist=0.0):

        end_cache = position
        Count = 0
        state = 1.0
        while Count < steps:
            Count = Count + 1
            end_cache = end_cache + np.array([self.min_step * np.sin(theta[0]), self.min_step * np.cos(theta[0])])
            end = np.mod(end_cache, self.period)

            position_integer = np.floor(end_cache / self.period).astype(np.int)
            if self.mat_exist[position_integer[0] + self.expand, position_integer[1] + self.expand] > 0:
                if np.linalg.norm(end - np.array([self.period / 2, self.period / 2])) - (self.radius + min_dist) <= 0:
                    state = np.linalg.norm(end_cache - position) / self.scope
                    break

        return state, end_cache

    def _prepare_background_for_render(self):
        small_mat_height = self.mat_height[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                           self.expand - self.margin: self.expand + self.num_circle + self.margin]
        small_mat_exist = self.mat_exist[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                          self.expand - self.margin: self.expand + self.num_circle + self.margin]
        index_tmp = [i - self.margin for i in range(np.shape(small_mat_height)[0])]
        position_tmp = list(itertools.product(index_tmp, index_tmp))
        position_tmp = [list(pos) for pos in position_tmp]
        position_tmp = np.array(position_tmp)
        position_tmp = position_tmp * self.period + self.period / 2

        cylinders = []
        small_mat_height = list(small_mat_height.reshape(1,-1)[0])
        small_mat_exist = list(small_mat_exist.reshape(1,-1)[0])
        position_tmp = list(position_tmp)

        for hei, exi, pos in zip(small_mat_height, small_mat_exist, position_tmp):
            if exi > 0:
                p1 = np.concatenate([pos, np.array([0])])
                p2 = np.concatenate([pos, np.array([hei])])
                r = self.radius
                cylinders.append([p1, p2, r])

        self.env_params['cylinders'] = copy.deepcopy(cylinders)
        # np.savetxt('./mat_height.txt', self.mat_height, fmt='%d', delimiter=' ', newline='\r\n')
        # np.savetxt('./mat_exist.txt', self.mat_exist, fmt='%d', delimiter=' ', newline='\r\n')

    def _get_observation(self, position, target, orient):
        global_counter = 0
        basic_counter = 0
        extra_counter = 0

        for dir in self.basic_directions:
            theta = np.mod(dir + orient, 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度
            # self.state[basic_counter], end_cache = self._range_finder(position, theta, int(self.scope/self.min_step))
            self.state[basic_counter], end_cache = self._fast_range_finder(position, theta, self.scope)
            self.end_points[global_counter] = [np.concatenate([position, np.array([self.level])]),
                                                   np.concatenate([end_cache, np.array([self.level])])]

            global_counter += 1
            basic_counter += 1

        # adding extra range finders
        for dir in self.extra_directions:
            theta = np.mod(dir + orient, 2 * np.pi)  # 计算传感器各个方向的角度,保证不超过360度
            # self.state[12 + extra_counter], end_cache = self._range_finder(position, theta, int(self.scope/self.min_step))
            self.state[12 + extra_counter], end_cache = self._fast_range_finder(position, theta, self.scope)
            self.end_points[global_counter] = [np.concatenate([position, np.array([self.level])]),
                                                   np.concatenate([end_cache, np.array([self.level])])]
            global_counter += 1
            extra_counter += 1

        # 保存目标的绝对方向角
        self.state[9] = np.sin(orient)  # normalization
        self.state[10] = np.cos(orient)  # normalization
        # 保存速度为另一个状态
        self.state[11] = 2*(self.speed / self.max_speed - 0.5)

        self.agent_params_pre = copy.deepcopy(self.agent_params)
        self.agent_params['position'] = copy.deepcopy(np.concatenate([position, np.array([self.level])]))
        self.agent_params['target'] = copy.deepcopy(np.concatenate([target, np.array([self.level])]))
        self.agent_params['rangefinders'] = copy.deepcopy(self.end_points)
        self.agent_params['direction'] = copy.deepcopy(np.mod(90 - orient/2/np.pi*360, 360))

    def step(self, action):
        assert self.is_reset, 'the environment must be reset before it is called'
        self._env_step_counter += 1
        position_temp = np.copy(self.position)
        self.orient = np.mod(self.real_action_range[0] * action[0] * np.pi + self.orient, 2 * np.pi)
        self.speed = np.where(action[1] >= 0,
                              self.speed + self.real_action_range[1] * action[1] * (-np.tanh(0.5 * (self.speed - self.max_speed))),
                              self.speed + self.real_action_range[1] * action[1] * np.tanh(0.5 * self.speed))

        iter_num = np.where(self.speed / self.min_step > np.floor(self.speed / self.min_step),
                            np.int(self.speed / self.min_step) + 1, np.int(self.speed / self.min_step))[0]

        # done1, end_cache = self._range_finder(np.copy(self.position), np.copy(self.orient), iter_num, self.min_distance_to_obstacle)
        done1, end_cache = self._fast_range_finder(np.copy(self.position), np.copy(self.orient), self.speed[0], self.min_distance_to_obstacle, 'forward')
        self.position = np.copy(end_cache)

        self._get_observation(np.copy(self.position), np.copy(self.target), np.copy(self.orient))
        done1 = True if done1 < 1.0 else False
        done2 = (np.linalg.norm(self.position - self.target) <= self.min_distance_to_target)
        done3 = (np.linalg.norm(self.position - self.target) >= 1e4)

        # terminal judgement ###########################################################################################
        done = done1 + done2 + done3
        # if done1:
        #     print('agent collides with obstacles!', iter_num, self.position)
        # if done2:
        #     print('agent arrived at the destination!')
        # if done3:
        #     print('agent is too far from the target position!')

        self.observation['observation'] = np.copy(self.state)
        self.observation['achieved_goal'] = (self.position / (self.num_circle * self.period) - 0.5) * 2

        # Reward #######################################################################################################
        if self.reward_type == "sparse":
            reward_sparse = np.where(done2, np.zeros([1]), np.zeros([1]) - 1.0)
            reward = reward_sparse[0]
        else:
            reward_sparse = np.where(done2, np.zeros([1]) + 10.0, np.zeros([1]))
            # reward_distance = np.tanh(0.2 * (10.0 - self.speed)) * (
            #             np.linalg.norm(position_temp - self.target) - np.linalg.norm(self.position - self.target))

            reward_distance = np.linalg.norm(position_temp - self.target) - np.linalg.norm(self.position - self.target)

            # reward_distance = np.max([np.linalg.norm(position_temp - self.target)
            #                           - np.linalg.norm(self.position - self.target), 0.0])

            reward_barrier = np.where(np.min(self.state[0:9]) * 100 <= 10.0, -5.0 + np.zeros([1]), np.zeros([1]))
            reward_action = -1.5 * np.ones([1])
            reward = (reward_sparse + reward_barrier + reward_distance + reward_action)[0] / 10.0

        if done2:
            info = {'is_success': 1.0}
        else:
            info = {'is_success': 0.0}

        if done:
            self.terminate_render = True

        return self.observation, reward, done, info

    def reset(self):
        self.is_reset = True
        self.first_render = True
        self.terminate_render = False
        self._env_step_counter = 0
        self.mat_height = np.random.randint(
            1, 10, size=(self.num_circle + 2 * self.expand, self.num_circle + 2 * self.expand)) * 17.0 + 30.0  # 建筑物高度
        W, H = np.shape(self.mat_height)
        for i in range(W):
            for j in range(H):
                if self.mat_height[i, j] > self.level + self.delta:
                    self.mat_height[i, j] = self.mat_height[i, j] + np.int(np.random.uniform(0, 200))
        self.mat_exist = self.mat_height - self.level  # 确定飞行高度范围内的建筑
        while True:
            position = np.random.uniform(0, self.period, size=(2,))
            if np.linalg.norm(position - np.array([self.period / 2, self.period / 2])) - (self.radius + self.min_distance_to_obstacle) > 0:
                break
        relative_position = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
        self.position = position + relative_position * self.period

        # ##############################其次产生目标位置#####################################

        while True:
            target = np.random.uniform(0, self.period, size=(2,))
            if np.linalg.norm(target - np.array([self.period / 2, self.period / 2])) - self.radius > 0:
                break

        while True:  # ensure that the minimum distance between initial position and target position is larger than 200m
            relative_target = np.random.randint(0, self.num_circle, size=(2,)).astype(np.float)
            target_temp = np.array(target + relative_target * self.period)
            if np.linalg.norm(target_temp - self.position) >= self.min_initial_starts:
                break
        self.target = target + relative_target * self.period
        #################################################################################################
        self.orient = np.random.uniform(0, 2 * np.pi, size=(1,))
        self.speed = np.zeros([1])
        self._prepare_background_for_render()
        #################################################################################################
        self._get_observation(np.copy(self.position), np.copy(self.target), np.copy(self.orient))

        self.observation['observation'] = np.copy(self.state)
        self.observation['achieved_goal'] = (self.position / (self.num_circle * self.period) - 0.5) * 2
        self.observation['desired_goal'] = (self.target / (self.num_circle * self.period) - 0.5) * 2

        return self.observation

    def render(self, mode='human'):
        sleep_time = 0.2
        print('orient={}, speed={}'.format(self.orient, self.speed))
        # self.File.write(str(self.target[0]))
        # self.File.write(' ')
        # self.File.write(str(self.target[1]))
        # self.File.write(' ')
        # self.File.write(str(self.position[0]))
        # self.File.write(' ')
        # self.File.write(str(self.position[1]))
        # self.File.write(' ')
        # for j in range(len(self.state)):
        #     self.File.write(str(self.state[j]))
        #     self.File.write(' ')
        # self.File.write(str(self.speed[0]))
        # self.File.write(' ')
        # self.File.write(str(self.orient[0]))
        # self.File.write('\n')

        assert self.is_reset, 'the environment must be reset before rendering'
        if self.first_render:
            time.sleep(sleep_time)
            self.first_render = False
            renderer = vtk.vtkRenderer()
            renderer.SetBackground(.2, .2, .2)
            # Render Window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.AddRenderer(renderer)
            renderWindow.SetSize(1600, 1600)
            self.Timer = TimerCallback(renderer)
            self.Timer.env_params = self.env_params

            def environment_render():
                renderWindowInteractor = vtk.vtkRenderWindowInteractor()
                renderWindowInteractor.SetRenderWindow(renderWindow)
                renderWindowInteractor.Initialize()
                renderWindowInteractor.AddObserver('TimerEvent', self.Timer.execute)
                timerId = renderWindowInteractor.CreateRepeatingTimer(30)
                self.Timer.timerId = timerId
                renderWindow.Render()
                renderWindowInteractor.Start()

            self.th = threading.Thread(target=environment_render, args=())
            # print(threading.Event())
            # self.event = threading.Event()
            self.th.start()
            time.sleep(1.0)
        else:
            self.Timer.terminate_render = self.terminate_render
            positions, directions = Smoother(self.agent_params_pre['position'], self.agent_params['position'],
                                             self.agent_params_pre['direction'], self.agent_params['direction'])
            # print(self.agent_params['position'])
            # print(self.agent_params['direction'])
            for i in range(len(positions)):
                time.sleep(sleep_time / len(positions))
                agent_params_tmp = copy.deepcopy(self.agent_params)
                agent_params_tmp['position'] = positions[i]
                agent_params_tmp['direction'] = directions[i]
                if i < len(positions) - 1:
                    agent_params_tmp['rangefinders'] = None
                self.Timer.agent_params = agent_params_tmp

            # if self.terminate_render:
            #     self.event.set()
            #
            #     print('threading is stopped', self.event.is_set())

    def compute_reward(self, achieved_goal, desired_goal, info):
        done = (np.linalg.norm(achieved_goal - desired_goal) <= self.min_distance_to_target)
        if done:
            return 0.0
        else:
            return -1.0

    def seed(self, seed=None):
        if seed:
            if seed >= 0:
                np.random.seed(seed)
    #
    # def close(self):
    #     pass


if __name__ == '__main__':
    env = UavGoalEnv()
    env.reset()
    for i in range(1000):
        action = env.action_space.sample()
        action[0] = 0.25 * action[0]
        obs, rew, done, info = env.step(action)
        print(rew, done, obs)
        env.render()

        if done:
            exit(0)
            env.reset()


