import gym
import logging
from gym import error, spaces, utils
from gym.spaces import Box
from gym.utils import seeding
import numpy as np
import time
import vtk
import threading
import itertools
import copy

from gym_uav.envs.utils import TimerCallback
from gym_uav.envs.utils import Smoother_soft
from gym_uav.envs.utils import dict2array
from gym_uav.envs.map import Map
from gym_uav.envs.vehicle import Vehicle


class UavBaseEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, config):
        logging.info("UAV base environment is called")
        # basic environment configurations
        # self.num_agents = num_agents
        self.basic_directions = config.basic_directions
        self.extra_directions = config.extra_directions
        self.directions = self.basic_directions + self.extra_directions

        # construct two classes: vehicle class and map class; vehicle class maintains UAV's position, orientation, speed
        # max speed and methods on executing actions

        # map class maintains the current environment map (obstacle type, distribution and height), and several methods
        # on obtaining distances between the UAV and obstacles

        # range finder parameters
        self.range_finder = {"max_scope": config.range_finder_max_scope,
                             "min_step": config.range_finder_min_step,
                             "basic_directions": config.range_finder_basic_directions,
                             "extra_directions": config.range_finder_extra_directions,
                             "total_directions":
                                 config.range_finder_basic_directions + config.range_finder_extra_directions,
                             }

        # UAV configurations

        self.uav_config = {"cruising_altitude": config.uav_cruising_altitude,
                                "position": np.zeros([self.num_agents, 3]),
                                "first_perspective": np.zeros([self.num_agents, 2]),
                                "orient2tar": np.zeros([self.num_agents, 2]),
                                "speed": np.zeros([self.num_agents, 2]),  # vertically and horizontally
                                "action_range": config.uav_action_range,
                                }
        
        self.vehicle = Vehicle(self.uav_config)

        # task information (including requirements to the UAV)
        self.task_information = {"departure": np.zeros([self.num_agents, 3]),
                                 "target": np.zeros([3]),
                                 "min_init_pos2tar": config.uav_min_init_pos2tar,
                                 "min_dist2tar": config.uav_min_dist2tar,
                                 "min_dist2obs": config.uav_min_dist2obs,
                                 "cruising_altitude_interval": config.uav_cruising_altitude_interval,
                                 "max_speed": config.uav_max_speed
                                 }
        #
        self.environment_config = {"obstacle_type": config.obstacle_type,
                                   "obstacle_base_height": config.obstacle_base_height,
                                   "obstacle_height_step": config.obstacle_height_step,
                                   "obstacle_max_step": config.obstacle_max_step,
                                   "obstacle_size": None,
                                   "basic_block_size": None,
                                   "basic_block_repeat_time": None,
                                   "obstacle_height_map": None,
                                   "obstacle_exist_map": None,
                                   "edge_expand_times": config.edge_expand_times
                                   }

        self.map = Map(self.environment_config)

        # self.state = {"range_finders": np.zeros([self.num_agents,
        #                                          len(self.range_finder['range_finder_total_directions'])]),
        #               "dist2tar": np.zeros([self.num_agents, 1]),
        #               "first_perspective": np.zeros([self.num_agents, self.uav_config['uav_orient2tar'].shape[1]]),
        #               "orient2tar": np.zeros([self.num_agents, self.uav_config['uav_orient2tar'].shape[1]]),
        #               "speed": np.zeros([self.num_agents, self.uav_config['uav_speed'].shape[1]]),
        #               }

        self.state = {}

        # self.action = {"horizontal_throttle": np.zeros([self.num_agents, 1]),
        #                "vertical_throttle": np.zeros([self.num_agents, 1]),
        #                "steer": np.zeros([self.num_agents, 1])}

        self.observation_space = Box(-np.inf, np.inf, [self._get_state().shape[0]], float)
        self.action_space = Box(-1.0, 1.0, [dict2array(self.vehicle.get_action()).shape[0]], float)
        self.reward_type = config.reward_type

        # self.render_config = {"step_counter": 0,
        #                       "position": None,
        #                       "target": None,
        #                       "direction": None,
        #                       "range_finders": None,
        #                       "first_render": False,
        #                       "terminate_render": False,
        #                       "camera_alpha": config.camera_alpha
        #                       }

        # self.agent_params = [{'position': self.position[j, :],
        #                       'target': self.target,
        #                       'direction':None,
        #                       'rangefinders': [None for _ in range(len(self.directions))], 'is_init':True}
        #                      for j in range(self.num_agents)]

        # self.agent_params_pre = None
        # self.render_config['terminate_render'] = False

        # other parameters
        # assert self.range_finder['max_scope'] >= self.uav_config['max_speed']
        assert self.reward_type in ["dense", "sparse"]

    def _prepare_background_for_render(self):
        small_mat_height = self.mat_height[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                           self.expand - self.margin: self.expand + self.num_circle + self.margin]
        small_mat_exist = self.mat_exist[self.expand - self.margin: self.expand + self.num_circle + self.margin,
                          self.expand - self.margin: self.expand + self.num_circle + self.margin]
        index_tmp = [i - self.margin for i in range(np.shape(small_mat_height)[0])]
        position_tmp = list(itertools.product(index_tmp, index_tmp))
        position_tmp = [list(pos) for pos in position_tmp]
        position_tmp = np.array(position_tmp)
        position_tmp = position_tmp * self.period + self.period / 2

        cylinders = []
        small_mat_height = list(small_mat_height.reshape(1,-1)[0])
        small_mat_exist = list(small_mat_exist.reshape(1,-1)[0])
        position_tmp = list(position_tmp)

        for hei, exi, pos in zip(small_mat_height, small_mat_exist, position_tmp):
            if exi > 0:
                p1 = np.concatenate([pos, np.array([0])])
                p2 = np.concatenate([pos, np.array([hei])])
                r = self.radius
                cylinders.append([p1, p2, r])

        self.env_params['cylinders'] = copy.deepcopy(cylinders)
        print("check", np.shape(self.position), np.shape(np.expand_dims(np.array([self.height] * self.num_agents), 0)))
        self.env_params['departure'] = copy.deepcopy(np.concatenate((self.position,
                                                                     np.expand_dims(np.array([self.height] * self.num_agents), 1)), 1))
        self.env_params['arrival'] = copy.deepcopy(np.concatenate([self.target, np.array([self.height])]))

    def _get_state(self):
        # self.agent_params_pre = copy.deepcopy(self.agent_params)

        destination = self.map.get_destination()
        range_finders = self.vehicle.get_range_finders()
        first_perspective = self.vehicle.get_first_perspective()
        speed = self.vehicle.get_speed()
        orient_to_destination = self.vehicle.get_orient_to_destination(destination)
        distance_to_destination = self.vehicle.get_distance_to_destination(destination)

        self.state = {}
        self.state.update({"range_finders": range_finders})
        self.state.update(({"first_perspective": first_perspective}))
        self.state.update({"speed": speed})
        self.state.update({"orient_to_destination": orient_to_destination})
        self.state.update({"distance_to_destination": distance_to_destination})

        return dict2array(self.state)

    def _compute_reward(self):
        destination, position = self.map.get_destination(), self.vehicle.get_position()
        # TODO: change threshold
        if np.linalg.norm(destination - position) < 10.0:
            return 1.0
        else:
            return 0.0

    def _is_done(self):
        # TODO: change threshold, add other "done" judgements
        range_finders = self.vehicle.get_range_finders()
        is_crash = (range_finders < 0.01).any()
        return is_crash

    def step(self, action):
        # assert self.render_config['first_render'], 'the environment must be reset before it is called'
        # self.render_config['step_counter'] += 1
        self.vehicle.shape_command(action)
        self.vehicle.move_forward()
        next_state = self._get_state()
        reward = self._compute_reward()
        done = self._is_done()

        # if done:
        #     self.render_config['terminate_render'] = True
        info = {}

        return next_state, reward, done, info

    def reset(self):
        # self.render_config['first_render'] = True
        # self.render_config['terminate_render'] = False
        # self.render_config['step_counter'] += 1

        # first reset the map (departure position and destination are also reset)
        self.map.reset()
        # then reset the vehicle
        self.vehicle.reset(self.map.get_departure())
        return self._get_state()

    def render(self, mode='human'):
        print("position in render", self.position)
        sleep_time = 0.2
        assert self.render_config['first_render'], 'the environment must be reset before rendering'
        if self.render_config['first_render']:
            time.sleep(sleep_time)
            self.render_config['first_render'] = False
            renderer = vtk.vtkRenderer()
            renderer.SetBackground(.2, .2, .2)
            # Render Window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.AddRenderer(renderer)
            renderWindow.SetSize(1600, 1600)
            self.Timer = TimerCallback(renderer)
            self.Timer.env_params = self.env_params

            def environment_render():
                renderWindowInteractor = vtk.vtkRenderWindowInteractor()
                renderWindowInteractor.SetRenderWindow(renderWindow)
                renderWindowInteractor.Initialize()
                renderWindowInteractor.AddObserver('TimerEvent', self.Timer.execute)
                timerId = renderWindowInteractor.CreateRepeatingTimer(30)
                self.Timer.timerId = timerId
                # renderWindow.Start()
                renderWindowInteractor.Start()

            self.th = threading.Thread(target=environment_render, args=())
            # print(threading.Event())
            # self.event = threading.Event()
            self.th.start()
            time.sleep(1.0)
        else:
            self.Timer.terminate_render = self.render_config['terminate_render']
            positions_list, directions_list = [], []
            division = 5
            for i in range(self.num_agents):
                positions, directions, _ = \
                    Smoother_soft(self.agent_params_pre[i]['position'], self.agent_params[i]['position'],
                                  self.orient_total_pre[i, 0],
                                  self.orient_total[i, 0],
                                  self.orient_render_pre[i, 0],
                                  self.orient_render[i, 0], divisions=division)
                for i in range(len(directions)):
                    directions[i] = np.array([np.mod(90 - np.mod(directions[i] / 2 / np.pi * 360.0, 360), 360)])
                directions_list.append(directions)
                positions_list.append(positions)

            directions_list = np.array(directions_list)
            positions_list = np.array(positions_list)

            positions_render = np.mean(positions_list, 0)
            orient_total_pre_mean = np.mean(self.orient_total_pre, 0)
            orient_total_mean = np.mean(self.orient_total, 0)
            orient_render_pre_mean = np.mean(self.orient_render_pre, 0)
            orient_render_mean = np.mean(self.orient_render, 0)

            _, _, directions_camera = Smoother_soft(None, None, orient_total_pre_mean, orient_total_mean,
                                                    orient_render_pre_mean, orient_render_mean, divisions=division)

            for i in range(len(directions_camera)):
                directions_camera[i] = np.array(
                    [np.mod(90 - np.mod(directions_camera[i] / 2 / np.pi * 360.0, 360), 360)])

            for j in range(division):
                for i in range(self.num_agents):
                    time.sleep(sleep_time / division / self.num_agents)
                    # print("agent={}, is init={}".format(i, self.agent_params[i]['is_init']))
                    # if self.agent_params[i]['is_init']:
                    #     time.sleep(0.1 / self.num_agents)
                    agent_params_tmp = copy.deepcopy(self.agent_params[i])
                    agent_params_tmp['position'] = positions_list[i, j]
                    agent_params_tmp['position_camera'] = positions_render[j]
                    agent_params_tmp['direction'] = directions_list[i, j, 0]
                    agent_params_tmp['direction_camera'] = directions_camera[j]
                    if j < division - 1:
                        agent_params_tmp['rangefinders'] = None
                    else:
                        agent_params_tmp['rangefinders'] = self.agent_params[i]['rangefinders']
                    self.Timer.agent_params = agent_params_tmp

            # if self.render_config['terminate_render']:
            #     self.event.set()
            #
            #     print('threading is stopped', self.event.is_set())

    def seed(self, seed=None):
        assert seed >= -1, "seed must be equal to or greater than -1 " \
                           "(-1 for random seeds and otherwise for specified seed)"
        if seed:
            if seed >= 0:
                np.random.seed(seed)

    def close(self):
        raise NotImplementedError


if __name__ == '__main__':
    config = None
    env = UavBaseEnv(config)

    env.reset()
    for i in range(1000):
        action = np.array([env.action_space.sample() for _ in range(agents)])
        # action[:, 0] = 0.25 * action[:, 0]
        obe, rew, done, info = env.step(action)
        env.render()
        if done:
            exit(0)
            env.reset()


