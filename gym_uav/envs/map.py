import numpy as np
import copy
import random


class Map:
    def __init__(self, args):
        self.obstacle_map = None  # maintains obstacle positions and heights

        self.obstacle_type = args.obstacle_type
        self.min_dist2tar = args.min_dist2tar
        self.departure = None
        self.destination = None

        assert self.obstacle_type in ['circle', 'square', 'trap', 'cross']
        if self.obstacle_type == 'circle':
            self.basic_obstacle_block = None
        elif self.obstacle_type == 'square':
            pass
        elif self.obstacle_type == 'trap':
            pass
        else:
            pass

    def compute_distance_to_obstacle(self, position, direction, scope=100.0):
        # TODO: debugs
        return 0.5

    def get_departure(self):
        return self.departure

    def get_destination(self):
        return self.destination

    def reset(self):
        # TODO: debugs
        self.obstacle_map = np.random.randint(
            1, 10,
            size=(self.num_circle + 2 * self.expand, self.num_circle + 2 * self.expand)) * self.delta + self.lowest

        position = np.zeros([15])
        return position
