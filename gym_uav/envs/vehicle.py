import time
import vtk
import copy
import numpy as np
from gym_uav.envs.utils import dict2array


class Vehicle:
    def __init__(self, args):
        self.position = {'x': None, 'y': None, 'z': None}
        self.prev_position = {}
        self.first_perspective = None
        self.speed = {'horizontal': None, 'vertical': None}
        self.range_finders = {}
        self.action = {"throttle_vertical": None, "throttle_horizontal": None, "steering": None}

        self.max_speed = args.max_speed
        self.action_range = args.action_range

    def shape_command(self, action):
        # TODO: keep consistency
        self.action["throttle_vertical"] = action[0]
        self.action["throttle_horizontal"] = action[1]
        self.action["steering"] = action[2]

    def move_forward(self):
        # TODO: fix speed update rule

        self.prev_position.update(self.position)
        self.speed['horizontal'] += self.action['throttle_horizontal']
        self.speed['vertical'] += self.action['throttle_vertical']
        self.first_perspective = np.mod(self.first_perspective + self.action['steer'], 2*np.pi)

        self.position['x'] += self.speed['horizontal'] * np.sin(self.first_perspective)
        self.position['y'] += self.speed['horizontal'] * np.cos(self.first_perspective)
        self.position['z'] += self.speed['vertical']

    def get_action(self):
        return self.action

    def get_range_finders(self):
        return self.range_finders

    def get_first_perspective(self):
        return self.first_perspective

    def get_position(self):
        return self.position

    def get_prev_position(self):
        return self.prev_position

    def get_speed(self):
        return self.speed

    def update_range_finders(self, distance):
        self.range_finders = distance

    def get_orient_to_destination(self, destination):
        return 0.0, 1.0

    def get_distance_to_destination(self, destination):
        distance = np.linalg.norm(dict2array(self.position) - destination)
        return distance

    def reset(self, position, first_perspective=None, speed=None, range_finders=None):
        # TODO: check dimensions; check range finders
        self.position = position

        if first_perspective is None:
            self.first_perspective = np.random.uniform(0, 2*np.pi)
        else:
            self.first_perspective = first_perspective

        if speed is None:
            self.speed = {'horizontal': np.zeros([1]), 'vertical': np.zeros([1])}
        else:
            self.speed = speed

        if range_finders is None:
            self.range_finders = {}
        else:
            self.range_finders.update(range_finders)

        self.action = {"throttle_vertical": None, "throttle_horizontal": None, "steering": None}
